// const API = 'https://randomuser.me/api/';
const API = process.env.API;

const getData = async (id) => {
  const apiURl = id ? `${API}${id}` : API;
  console.log({
    todos_env: process.env,
    test: process.env.TEST_REACT
  })
  try {
    const response = await fetch(apiURl);
    const data = await response.json();
    return data.results[0];
  } catch (error) {
    console.log('Fetch Error', error);
  };
};

export default getData;